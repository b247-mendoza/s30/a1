// 1. Using count operator to count the total number of fruits on sale.
db.fruits.aggregate([
	{ $match: {onSale: true} }, 
	{ $count: "fruitsOnSale" }
]);

// 2. Using count operator to count the total number of fruits with stock more than or equal to 20.

db.fruits.aggregate([
	{ $match: {stock: {$gte: 20} } }, 
	{ $count: "enoughStock" }
]);

// 3. Using average operator to get the average price of fruits onSale per supplier

db.fruits.aggregate([
	{$group: {_id: "$supplier_id", 
	avg_price: { $avg: "$price" } } }
]);

// 4. Using max operator to get the highest price of fruit per supplier

db.fruits.aggregate([
	{$group: {_id: "$supplier_id", 
	max_price: { $max: "$price" } } }
]);

// 5. Using min operator to get the lowest price of the fruit per supplier

db.fruits.aggregate([
	{$group: {_id: "$supplier_id", 
	min_price: { $min: "$price" } } }
]);